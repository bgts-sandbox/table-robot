# TABLE ROBOT beta 

## BEHAVIOUR:
### Table Robot Can: 
	- Move around the Table (grid)
	- Face North,East,West, and South Directions
	- Not fall from table (see UNIT_TEST_5 and UNIT_TEST_6)
	- Report current location and Direction
	- Ignore any commands until there is a valid PLACE Command (see UNIT_TEST_8)

	
## REQUIREMENTS:
### Requires *.conf Configuration file to read the COMMANDS options
```COMMANDS=PLACE=x:y:f,[MOVE,LEFt,RIGHT,REPORT...]```


### Sample Configuration file ```/path/to/robot.conf``` contents

```
[TEST_01]
COMMANDS=PLACE=0:0:NORTH,MOVE,LEFT,RIGHT,REPORT
```

### Available Commands:(command - description - output)

```
PLACE - Sets the coordinates and the direction of the robot. (PLACE=0:0:NORTH) - Boolean
MOVE - Move 1 step to the direction the robot is facing. (MOVE) - x+-1, y+-1
LEFT / RIGHT - Rotate robot's direction (+/-)90 degrees. (LEFT / RIGHT) - N->W / N -> E
REPORT - States the current coordinates and direction of the robot. (REPORT) - [0,0,NORTH]
```


## USAGE:
```python robot.py robot.conf```

## USAGE OOP
```python ./oop/run.py ./oop/robot.conf```