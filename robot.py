#################################################################################################
#################################################################################################
##
## TABLE ROBOT beta 
## 
## Can move around the Table (grid)
## Can face North,East,West, and South Directions
## Cannot fall from table (see UNIT_TEST_5 and UNIT_TEST_6)
## Can Report current location and Direction
## Will ignore any commands until there is a valid PLACE Command (see UNIT_TEST_8)
## 
## Requires *.conf Configuration file to read the COMMANDS options
## COMMANDS=PLACE=x:y:f,[MOVE,LEFt,RIGHT,REPORT...]
##
## Sample Configuration file /path/to/robot.conf
## [TEST_01]
## COMMANDS=PLACE=0:0:NORTH,MOVE,LEFT,RIGHT,REPORT
##
##
## Available Commands:(command - description - output)
## PLACE - Sets the coordinates and the direction of the robot. (PLACE=0:0:NORTH) - Boolean
## MOVE - Move 1 step to the direction the robot is facing. (MOVE) - x+-1, y+-1
## LEFT / RIGHT - Rotate robot's direction (+/-)90 degrees. (LEFT / RIGHT) - N->W / N -> E
## REPORT - States the current coordinates and direction of the robot. (REPORT) - [0,0,NORTH]
##
#################################################################################################
#################################################################################################


import sys
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(sys.argv[1]))


#Indexed grid
grid  = [4,4]
#Circular index
faces = ["NORTH","EAST","SOUTH","WEST"]
#Default position
botpos = []

#Validates if the x y and face is within constraints
def validate(arr):
	if(arr[0]<=grid[0] and arr[0]>=0 and arr[1]<=grid[1] and arr[1]>=0 and arr[2] in faces):
		return True
	return False

#Places the robot on the table	
def place(arr):
	global botpos
	arr[0] = int(arr[0])
	arr[1] = int(arr[1])
	if(validate(arr)):
		botpos = arr
		return True
	else:
		print("Invalid Placement")
		return False

#Moves the robot 1 place up facing a particular direction
def move():
	global botpos
	eot = "Edge of Table"
	if(botpos[2] == "EAST"):
		if(botpos[0] < grid[0] and validate(botpos)):
			botpos[0]+=1
			return "x+1" 
		return eot
	elif(botpos[2] == "WEST"):
		if(botpos[0] > 0 and validate(botpos)):
			botpos[0]-=1
			return "x-1"
		return eot
	elif(botpos[2] == "NORTH"):
		if(botpos[1] < grid[1] and validate(botpos)):
			botpos[1]+=1
			return "y+1"
		return eot
	elif(botpos[2] == "SOUTH"):
		if(botpos[1] > 0 and validate(botpos)):
			botpos[1]-=1
			return "y-1"
		return eot
	else:
		print(eot)
		return False
		
#Rotate the relative direction of the robot to the left - N > W > S > E > N ...
def left():
	global botpos
	prev = botpos[2]
	fI = faces.index(botpos[2])
	if(fI>0):
		botpos[2]=faces[fI-1]
	elif(fI-1<0):
		botpos[2]=faces[len(faces)-1]
	return prev + " -> " + botpos[2]
	
#Rotate the relative direction of the robot to the right - N > E > S > W > N ...
def right():
	global botpos
	prev = botpos[2]
	fI = faces.index(botpos[2])
	if(fI+1<len(faces)):
		botpos[2]=faces[fI+1]
	else:
		botpos[2]=faces[0]
	return prev + " -> " + botpos[2]

#Reports the current position and direction of the robot
def report():
	global botpos
	return("OUTPUT: " + str(botpos))


#Method mapping
dispatch = {
	'PLACE':place,
	'MOVE':move,
	'LEFT':left,
	'RIGHT':right,
	'REPORT':report
}

#Read and run the commands from the configuration file, with movement guides
for section in config.sections():
	print("====================================")
	print(section)
	print("====================================")
	botpos=[]
	options = config.options(section)
	CONFIG_COMMANDS = config.get(section,"COMMANDS")
	COMMANDS = CONFIG_COMMANDS.split(',')
	for cmd in COMMANDS:
		print(cmd)
		try:
			method = cmd.split('=')
			if(len(method)>1):
				param = method[1].split(':')
				print(dispatch[method[0]](param))
			else:
				print(dispatch[method[0]]())
		except:
			print("Invalid Command")
			
	
	