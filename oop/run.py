import sys
import ConfigParser
import robot

config = ConfigParser.ConfigParser()
config.readfp(open(sys.argv[1]))
bot = robot.Robot()

#Method mapping
dispatch = {
	'PLACE':bot.place,
	'MOVE':bot.move,
	'LEFT':bot.left,
	'RIGHT':bot.right,
	'REPORT':bot.report
}

#Read and run the commands from the configuration file, with movement guides
for section in config.sections():
	print("====================================")
	print(section)
	print("====================================")
	bot.botpos=[]
	options = config.options(section)
	CONFIG_COMMANDS = config.get(section,"COMMANDS")
	COMMANDS = CONFIG_COMMANDS.split(',')
	for cmd in COMMANDS:
		print(cmd)
		try:
			method = cmd.split('=')
			if(len(method)>1):
				param = method[1].split(':')
				print(dispatch[method[0]](param))
			else:
				print(dispatch[method[0]]())
		except:
			print("Invalid Command")
			
	
	