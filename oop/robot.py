#################################################################################################
#################################################################################################
##
## TABLE ROBOT beta 
## 
## Can move around the Table (grid)
## Can face North,East,West, and South Directions
## Cannot fall from table (see UNIT_TEST_5 and UNIT_TEST_6)
## Can Report current location and Direction
## Will ignore any commands until there is a valid PLACE Command (see UNIT_TEST_8)
## 
## Requires *.conf Configuration file to read the COMMANDS options
## COMMANDS=PLACE=x:y:f,[MOVE,LEFt,RIGHT,REPORT...]
##
## Sample Configuration file /path/to/robot.conf
## [TEST_01]
## COMMANDS=PLACE=0:0:NORTH,MOVE,LEFT,RIGHT,REPORT
##
##
## Available Commands:(command - description - output)
## PLACE - Sets the coordinates and the direction of the robot. (PLACE=0:0:NORTH) - Boolean
## MOVE - Move 1 step to the direction the robot is facing. (MOVE) - x+-1, y+-1
## LEFT / RIGHT - Rotate robot's direction (+/-)90 degrees. (LEFT / RIGHT) - N->W / N -> E
## REPORT - States the current coordinates and direction of the robot. (REPORT) - [0,0,NORTH]
##
#################################################################################################
#################################################################################################


class Robot:
	#Indexed grid
	grid  = [4,4]
	#Circular index
	faces = ["NORTH","EAST","SOUTH","WEST"]
	#Default position
	botpos = []

	#Validates if the x y and face is within constraints
	def validate(self,arr):
		if(arr[0]<=self.grid[0] and arr[0]>=0 and arr[1]<=self.grid[1] and arr[1]>=0 and arr[2] in self.faces):
			return True
		return False

	#Places the robot on the table	
	def place(self,arr):
		#global self.botpos
		arr[0] = int(arr[0])
		arr[1] = int(arr[1])
		if(self.validate(arr)):
			self.botpos = arr
			return True
		else:
			print("Invalid Placement")
			return False

	#Moves the robot 1 place up facing a particular direction
	def move(self):
		#global self.botpos
		eot = "Edge of Table"
		if(self.botpos[2] == "EAST"):
			if(self.botpos[0] < self.grid[0] and self.validate(self.botpos)):
				self.botpos[0]+=1
				return "x+1" 
			return eot
		elif(self.botpos[2] == "WEST"):
			if(self.botpos[0] > 0 and self.validate(self.botpos)):
				self.botpos[0]-=1
				return "x-1"
			return eot
		elif(self.botpos[2] == "NORTH"):
			if(self.botpos[1] < self.grid[1] and self.validate(self.botpos)):
				self.botpos[1]+=1
				return "y+1"
			return eot
		elif(self.botpos[2] == "SOUTH"):
			if(self.botpos[1] > 0 and self.validate(self.botpos)):
				self.botpos[1]-=1
				return "y-1"
			return eot
		else:
			print(eot)
			return False
			
	#Rotate the relative direction of the robot to the left - N > W > S > E > N ...
	def left(self):
		#global self.botpos
		prev = self.botpos[2]
		fI = self.faces.index(self.botpos[2])
		if(fI>0):
			self.botpos[2]=self.faces[fI-1]
		elif(fI-1<0):
			self.botpos[2]=self.faces[len(self.faces)-1]
		return prev + " -> " + self.botpos[2]
		
	#Rotate the relative direction of the robot to the right - N > E > S > W > N ...
	def right(self):
		#global self.botpos
		prev = self.botpos[2]
		fI = self.faces.index(self.botpos[2])
		if(fI+1<len(self.faces)):
			self.botpos[2]=self.faces[fI+1]
		else:
			self.botpos[2]=self.faces[0]
		return prev + " -> " + self.botpos[2]

	#Reports the current position and direction of the robot
	def report(self):
		#global self.botpos
		return("OUTPUT: " + str(self.botpos))


